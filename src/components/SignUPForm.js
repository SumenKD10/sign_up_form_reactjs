import React, { Component } from "react";
var validator = require("validator");

class SignUPForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      age: "",
      gender: "",
      mailGiven: "",
      selectRole: "",
      checkedBox: "",
      password: "",
      repeatPassword: "",
      onSubmission: "Not_Submitted",
      error: {},
    };
  }

  handleChange = (event) => {
    let { name, value } = event.target;

    this.setState({
      [name]: value,
    });
    this.validateField(name, value);
  };

  validateField(name, value) {
    let { error } = this.state;
    let errorMessage = "";
    if (name === "firstName" && !validator.isAlpha(value)) {
      errorMessage = "First Name should only contain letters.";
    } else if (name === "lastName" && !validator.isAlpha(value)) {
      errorMessage = "Last Name should only contain letters.";
    } else if (
      name === "age" &&
      (!validator.isInt(value) || !(value >= 18 && value <= 35))
    ) {
      errorMessage = "Invalid Age, Age must be between 18 and 35";
    } else if (name === "mailGiven" && !validator.isEmail(value)) {
      errorMessage = "Invalid Mail Id";
    } else if (name === "password" && !validator.isStrongPassword(value)) {
      errorMessage =
        "Please enter a Password with minimum 8 characters having atleast 1 special character, 1 Upper Case and 1 lowercase";
    } else if (name === "repeatPassword" && !(value === this.state.password)) {
      errorMessage = "Password Doesn't Match!";
    } else {
      delete error[name];
      this.setState({
        error,
      });
    }

    if (errorMessage) {
      this.setState({
        error: {
          ...error,
          [name]: errorMessage,
        },
      });
    } else {
      return;
    }
  }

  handleClick = (event) => {
    window.location.reload();
  };

  handleEvent = (event) => {
    const { error } = this.state;
    let checkError = Object.values(error).filter((eachError) => {
      return eachError.length !== 0;
    });
    if (checkError.length === 0) {
      console.log("Submit Successful");
      this.setState({ onSubmission: "Submitted" });
    } else {
      console.log("Failed");
    }
    event.preventDefault();
    console.log(this.state.onSubmission);
  };

  render() {
    const { error } = this.state;
    return (
      <div className="d-flex justify-content-center p-5">
        <div className="col-xl-4 bg-light">
          <form
            className="card-body border border-secondary rounded-4 overflow-hidden "
            onSubmit={this.handleEvent}
          >
            <header className="p-4 bg-dark text-light text-center font-weight-bold border-top-rounded">
              <h3>Do-Shop</h3>
            </header>
            {this.state.onSubmission === "Submitted" && (
              <>
                <h3 className="p-5 text-success">
                  Sign Up is successfull, The confirmation message has been sent
                  to your email id.
                </h3>
                <div className="d-flex justify-content-center">
                  <button
                    className="btn btn-dark mb-3"
                    id="submitButton"
                    type="button"
                    onClick={this.handleClick}
                  >
                    Go Back
                  </button>
                </div>
              </>
            )}
            {this.state.onSubmission === "Not_Submitted" && (
              <div className="p-4">
                <h1 className="mb-4">Create Account</h1>
                <div className="input-group">
                  <span
                    className="input-group-text "
                    id="inputGroup-sizing-default"
                  >
                    <i className="fa-solid fa-user px-2"></i> First Name
                  </span>
                  <input
                    name="firstName"
                    type="text"
                    className="form-control"
                    aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default"
                    onChange={this.handleChange}
                  ></input>
                </div>
                {error.firstName && (
                  <small className="text-danger">{error.firstName}</small>
                )}
                <div className="input-group mt-3">
                  <span
                    className="input-group-text"
                    id="inputGroup-sizing-default"
                  >
                    <i className="fa-solid fa-people-roof px-2"></i>
                    Last Name
                  </span>
                  <input
                    type="text"
                    name="lastName"
                    className="form-control"
                    aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default"
                    onChange={this.handleChange}
                  ></input>
                </div>
                {error.lastName && (
                  <small className="text-danger">{error.lastName}</small>
                )}
                <div className="input-group mt-3">
                  <span
                    className="input-group-text"
                    id="inputGroup-sizing-default"
                  >
                    <i className="fa-solid fa-calendar-days px-2"></i>
                    Age
                  </span>
                  <input
                    type="text"
                    name="age"
                    className="form-control"
                    aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default"
                    onChange={this.handleChange}
                  ></input>
                </div>
                {error.age && (
                  <small className="text-danger">{error.age}</small>
                )}
                <div className="genderDiv d-flex gap-4 mt-2">
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadioDefault"
                      id="flexRadioDefault1"
                      value="male"
                    ></input>
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault1"
                    >
                      Male
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadioDefault"
                      id="flexRadioDefault2"
                      value="female"
                      required
                    ></input>
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault2"
                    >
                      Female
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadioDefault"
                      value="others"
                      id="flexRadioDefault3"
                    ></input>
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault3"
                    >
                      Others
                    </label>
                  </div>
                </div>
                <div>
                  <select
                    className="form-select form-select mt-3"
                    aria-label=".form-select-lg example"
                    name="selectRole"
                  >
                    <option value="1">Developer</option>
                    <option value="2">Senior Developer</option>
                    <option value="3">Lead Engineer</option>
                    <option value="4">CTO</option>
                  </select>
                </div>

                <div className="form-floating mt-3">
                  <input
                    name="mailGiven"
                    type="email"
                    className="form-control"
                    id="floatingInput"
                    placeholder="name@example.com"
                    onChange={this.handleChange}
                  ></input>
                  <label htmlFor="floatingInput">Email Address</label>
                </div>
                {error.mailGiven && (
                  <small className="text-danger">{error.mailGiven}</small>
                )}
                <div className="form-floating mt-3">
                  <input
                    type="password"
                    name="password"
                    className="form-control"
                    id="floatingPassword1"
                    placeholder="Password"
                    onChange={this.handleChange}
                  ></input>
                  <label htmlFor="floatingPassword1">Password</label>
                </div>
                {error.password && (
                  <small className="text-danger">{error.password}</small>
                )}
                <div className="form-floating mt-3">
                  <input
                    type="password"
                    className="form-control"
                    id="floatingPassword2"
                    placeholder="Password"
                    name="repeatPassword"
                    onChange={this.handleChange}
                  ></input>
                  <label htmlFor="floatingPassword2">Repeat Password</label>
                </div>
                {error.repeatPassword && (
                  <small className="text-danger">{error.repeatPassword}</small>
                )}
                <div className="mt-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    value="checkForTerms"
                    name="checkedBox"
                    id="flexCheckDefault"
                    required
                  ></input>
                  <label
                    className="form-check-label px-2"
                    htmlFor="flexCheckDefault"
                  >
                    I agree to the terms and condition.
                  </label>
                </div>
                <div className="d-flex justify-content-center">
                  <button
                    className="btn btn-dark"
                    id="submitButton"
                    type="submit"
                  >
                    Submit
                  </button>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    );
  }
}

export default SignUPForm;
