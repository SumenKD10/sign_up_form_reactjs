import React, { Component } from "react";
import SignUPForm from "./components/SignUPForm";
import "./App.css";

class App extends Component {
  render() {
    return (
      <>
        <SignUPForm />
      </>
    );
  }
}

export default App;
